const route = Rlite(notFound, {
    // Default route
    '': function () {
        return getPage('home');
    },
  

    'add-top': function () {
        return getPage('addTop');
    },
 
    // #inbox
    'top/:id': function ({id}) {
        return getPage('showTop');
    },
    'home': function () {
        return getPage('home');
    },
 
  });
  
  function notFound() {
    return '<h1>404 Not found :/</h1>';
  }
  
  // Hash-based routing
  function processHash() {
    const hash = location.hash || '#';
    route(hash.slice(1)).then(res =>  $("#app").html(res))
  }
  
  window.addEventListener('hashchange', processHash);
  processHash();

  async function getPage(path) {
    let html = await fetch('/pages/'+path+'.html')
    html = await html.text()
   
    return html;
  } 