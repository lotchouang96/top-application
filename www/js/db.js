

/**
 * @param {string} title 
 * @param {int} position
 * @param {string} image
 **/
function createElt(title, position, image)
{
    return new Elt(title, position, image)
}

(async ()=>{
    if(getAllTops().length === 0 )
    {
        const topCar = new Top('Top voiture de saison formula 1 2021', 'Ici vous trouverz le top des voitures les plus performantes de saison 2020/2021' +
            'de la formule 1');


        let car1 = await getImgPath('/images/car/1.jpg');
        let car2 = await getImgPath('/images/car/2.jpg');
        let car3 = await getImgPath('/images/car/3.jpg');

        topCar
            .addElement(createElt('Mac laren', 2, car2))
            .addElement(createElt('La AMG de l\'écurie mercedes', 1, car1))
            .addElement(createElt('RedBull', 3, car3));

        let driver = await getImgPath('/images/f1/1.jpg');
        let driver2 = await getImgPath('/images/f1/2.jpg');
        let driver3 = await getImgPath('/images/f1/3.jpg');
        const topDriver = new Top('Mon top des pilotes de F1 2020/2021', 'ceci est ma liste de top des pilote de course de la F1 de');
        topDriver
            .addElement(createElt('lewis hamilton', 1, driver))
            .addElement(createElt('max verstappen', 2, driver2))
            .addElement(createElt('Lando Norris', 3, driver3));
        save(topCar);
        save(topDriver);
    }
})()

/** @param{string} id 
 * @returns {(Top | null)}
*/
function findTopById(id){
    let tabTops = getAllTops();
    for (let i = 0; i< tabTops.length; i++)
    {
        if(tabTops[i].id === id)
        {
            return tabTops[i];
        }
    }

    return null;
}

/**
 *  @returns {Array<Top>}
 */
function getAllTops()
{
    return JSON.parse(localStorage.getItem('tops')) || [];
}

/**
 * 
 * @param {Top} top 
 */
function save (top)
{
    console.log(top)
    let tops = JSON.parse(localStorage.getItem('tops')) || [];
    tops.push(top)
    localStorage.setItem('tops', JSON.stringify(tops))
}

/**
 * @param {string} path
 * @returns {Promise<string>}
 */
 async function getImgPath(path)
{
    let img;
    img = fetch(path)
        .then((e) => e.blob())
        .then((blob) => fileTOBase64(new File([blob],'2.jpg',{type: blob.type})))
    return await img;
}

/**
 * @param file
 * @returns {Promise<string>}
 */
async function fileTOBase64(file)
{
    return new Promise((resolve, reject) =>{
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = (e) => reject(e);
    })
}

function clearLocalStorage() {
    localStorage.removeItem('tops');
}