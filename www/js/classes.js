class Elt  {
    
    /**
     * @param {string} title
     * @param {int} position
     * @param {string} image
     **/
    constructor(title,position,image)
    {
        this.title = title;
        this.position = position;
        this.image = image
    }
    
}

class Top{
    
    /**
     * @param {string} title
     * @param {string} description
     **/
    constructor(title, description)
    {
        /** @param{string} id*/
        this.id = Math.random().toString(36).substr(2, 9)
        this.title = title;
        this.description = description;
        this.elts = [];
    }

    /** 
     * @param {Elt} elt
     * 
     **/ 
    addElement(elt){
        this.elts.push(elt)
        return this;
    }

}
